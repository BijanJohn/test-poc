# test-poc

This is an example Gitlab repo/pipeline that creates a Docker Image that is stored in the Gitlab Container Registry.

The idea behind this is to add the needed tools and technologies to a shared image so that it can be used to build other tools.
