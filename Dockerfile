FROM fedora:36
LABEL maintainer="Bijan Rahnamai <bijanrahnamai@gmail.com>"

ARG ACCOUNT_NAME="builder"
ARG ACCOUNT_HOME="/home/builder"

ARG NOMAD_VERSION="1.4.2"
ARG PACKER_VERSION="1.8.4"
ARG TERRAFORM_VERSION="1.3.4"
ARG VAULT_VERSION="1.12.1"

ARG NPM_REGISTRY="https://registry.npmjs.org"

ARG POETRY_VERSION="1.2.1"
ARG POETRY_HOME="/usr/local"

ARG SUPPLEMENTAL_PYTHON_VERSION="3.10"

# create a builder user and group that the container will run as
RUN groupadd -r -g 987 ${ACCOUNT_NAME} \
    && useradd -r -u 987 -m -s /bin/bash -g ${ACCOUNT_NAME} ${ACCOUNT_NAME} \
    && groupadd -r -g 357 docker

# copy hashicorp software installer scripts
COPY files/hashicorp_software_install.sh /usr/local/bin/hashicorp_software_install.sh

# copy awscli installer script
COPY files/awscliv2_install.sh /usr/local/bin/awscliv2_install.sh

# user directory setup
COPY files/ansible.cfg ${ACCOUNT_HOME}/.ansible.cfg
RUN mkdir -m 755 -p ${ACCOUNT_HOME}/.ansible/plugins/modules \
 && mkdir -m 755 -p ${ACCOUNT_HOME}/.packer.d/plugins \
 && mkdir -m 700 -p ${ACCOUNT_HOME}/.ssh \
 && chown ${ACCOUNT_NAME}:${ACCOUNT_NAME} -R ${ACCOUNT_HOME}
COPY files/ssh_config ${ACCOUNT_HOME}/.ssh/config
RUN chown ${ACCOUNT_NAME}:${ACCOUNT_NAME} ${ACCOUNT_HOME}/.ssh/config && chmod 600 ${ACCOUNT_HOME}/.ssh/config


# install rpm packages
ARG CACHEBUST=1
RUN dnf update --setopt=install_weak_deps=false -y \
 && dnf install --setopt=install_weak_deps=false --nodocs -y \
    ansible \
    azure-cli \
    buildah \
    containerd \
    docker \
    findutils \
    freetds-devel \
    fuse-overlayfs \
    gcc \
    genisoimage \
    git \
    gpg \
    jq \
    npm \
    openssl \
    perl-Image-ExifTool \
    pipenv \
    podman \
    python3 \
    python${SUPPLEMENTAL_PYTHON_VERSION} \
    python3-ansible-lint \
    python3-devel \
    python3-flake8 \
    python3-molecule \
    python3-netaddr \
    python3-pip \
    python3-pytest \
    python3-pytest-xdist \
    python3-pyvmomi \
    python3-requests \
    python3-testinfra \
    python3-typer+all \
    python3-virtualenv \
    python3-winrm \
    libffi-devel \
    rsync \
    ShellCheck \
    skopeo \
    sshpass \
    sudo \
    unzip \
    wget \
    yamllint \
    zip \
 && dnf clean all

# install npm packages
RUN npm config set registry ${NPM_REGISTRY} && \
    npm install -g \
    pyright

# give permission to .sh files
RUN chmod 755 /usr/local/bin/awscliv2_install.sh
RUN chmod 755 /usr/local/bin/hashicorp_software_install.sh

# hashi install
RUN /usr/local/bin/hashicorp_software_install.sh "nomad" ${NOMAD_VERSION} \
    && /usr/local/bin/hashicorp_software_install.sh "packer" ${PACKER_VERSION} \
    && /usr/local/bin/hashicorp_software_install.sh "terraform" ${TERRAFORM_VERSION} \
    && /usr/local/bin/hashicorp_software_install.sh "vault" ${VAULT_VERSION}

# awscli
RUN /usr/local/bin/awscliv2_install.sh

# Python - Poetry, package management
# Run the installer and cleanuop install dir to avoid eating space
RUN git clone "https://github.com/python-poetry/install.python-poetry.org.git" /tmp/install-poetry \
    && python3.10 /tmp/install-poetry/install-poetry.py --git "https://github.com/python-poetry/poetry.git@${POETRY_VERSION}" \
    && rm -rf /tmp/install-poetry

# runtime argument
USER ${ACCOUNT_NAME}